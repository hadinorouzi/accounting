const http = require("http");

const hostname = "localhost";
const port = 5050;
http
  .createServer((req, res) => {
    const { url, method } = req;
    const body = [];
    if (url === "/signup" && method === "POST") {
      req.on("data", chunk => {
        body.push(chunk);
      });
      req.on("end", () => {
        let data = Buffer.concat(body).toString();
        res.statusCode = 200;
        res.setHeader("Content-type", "application/json");

        res.end(data);
      });
    }
  })
  .listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
  });
