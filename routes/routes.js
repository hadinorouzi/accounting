const fs = require("fs");
const path = require("path");
const signup = require("../controller/singup");

const routes = (req, res) => {
  const { url, method } = req;
  if (url === "/" && method === "GET") {
  } else if (url === "/signup" && method === "POST") {
    // bodyPars(res).then(console.log);
    let body;

    res.on("data", data => {
      console.log("hi");
      body = data;
    });
    res.on("end", () => {
      const data = Buffer.concat(body).toString();
      console.log(data);
      signup(data);
      return res.end("ok");
    });
  } else {
    res.statusCode = 404;
    res.setHeader("Content-type", "text/html");
    fs.readFile("/Users/hadi/Desktop/server/index.html", (err, data) => {
      return res.end(data);
    });
  }
};

const bodyPars = async res => {
  const body = [];
  await res.on("data", data => {
    body.push(data);
  });
  await res.on("end", () => {
    return Buffer.concat(body).toString();
  });
};

module.exports = routes;
